import React from 'react';


export const DemoComponentPanel: React.FC = () => {
  return (
    <>
      <Panel title="Hello Panel">
        <input type="text" className="form-control" defaultValue="something..."/>
      </Panel>
      <br/>
      <Panel>
        Panel with no title
      </Panel>
    </>
  )
};


/////////////////////////
// PANEL COMPONENT
/////////////////////////

interface PanelProps {
  title?: string;
  children: React.ReactNode
}

const Panel: React.FC<PanelProps> = ({ title, children }) => {
  return (
    <div className="card">
      { title && <div className="card-header">{title}</div> }
      <div className="card-body">{children}</div>
    </div>
  )
};

