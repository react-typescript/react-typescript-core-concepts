import React, { useEffect, useRef } from 'react'

export const DemoUnControlledForm: React.FC = () => {
  const inputEl = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputEl.current && inputEl.current.focus()
  });

  const onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if(e.key === 'Enter') {
      alert(e.currentTarget.value);
      e.currentTarget.value = '';
    }
  };

  return (
    <div className="example-box-centered">

      <h6>useRef, useEffect, input Keyboard Events</h6>

      <input
        className="form-control"
        ref={inputEl} type="text"
        onKeyPress={onKeyPress}
        placeholder="Write something and press enter"
      />

    </div>
  )
};
