import React from 'react'

export const DemoPropsCallback: React.FC = () => {

  function openURL(url: string) {
    window.open(url);
  }

  function changeRoute(path: string) {
    window.location.href = path;
  }

  return (
    <div className="example-box-centered small">
      <Panel
        title="Props Callback Demo: go to link"
        icon="fa fa-link"
        onIconClick={() => openURL('http://www.google.com')}
      > Click Icon on header </Panel>

      <hr/>

      <Panel
        title="Props Callback Demo: change route"
        icon="fa fa-backward"
        onIconClick={() => changeRoute('../../')}
      > Click Icon on header </Panel>

    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH ICON CALLBACK
// =========================================

interface PanelProps {
  title ? : string;
  children?: React.ReactNode;
  icon?: string;
  onIconClick: () => void;
}

export const Panel: React.FC<PanelProps> = ({
  icon, title, onIconClick, children
}) => {

  const iconCls = `${icon} pull-right cursor`;

  return (
    <div className="card">
      <div className="card-header">
        { title }
        { icon && <i className={iconCls} onClick={onIconClick} />}
      </div>
      <div className="card-body">{children}</div>
    </div>
  )
};
