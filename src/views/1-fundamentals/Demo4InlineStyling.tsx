import React from "react"

export const DemoInlineStyling: React.FC = () => {
  return (
    <div style={{ width: '200px', textAlign: 'center', margin: '0 auto'}}>
      <Panel
        title="hello"
        text="How are you?"
        headerStyles={{ fontSize: '40px', color: 'white', backgroundColor: '#222'}}
        bodyStyles={{ fontSize: '15px', color: '#666'}}
      />
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH INLINE STYLE
// =========================================

interface PanelProps {
  title?: string;
  text?: React.ReactNode;
  headerStyles?: React.CSSProperties
  bodyStyles?: React.CSSProperties
}

export const Panel: React.FC<PanelProps> = props => {
  const css = {
    header: {
      backgroundColor: '#ccc',
      ...props.headerStyles,
    },
    body: props.bodyStyles
  };
  return (
    <div className="card">

      {
        props.title &&
        <div className="card-header" style={css.header}>{props.title}</div>
      }
      <div className="card-body" style={css.body}>{props.text}</div>
    </div>
  )
};
