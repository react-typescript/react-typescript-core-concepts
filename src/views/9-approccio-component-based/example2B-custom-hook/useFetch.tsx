import { Product } from '../shared/model/product';
import React, { useEffect } from 'react';
import { productsReducer } from '../example2A-useReducer/store/products.reducer';
import { activeReducer } from '../example2A-useReducer/store/active.reducer';

export function useFetch<T>(
  url: string,
  defaultActiveProduct: Product,
  defaultProducts: Product[]
) {

  const [products, dispatchProducts] = React.useReducer(productsReducer, defaultProducts);
  const [active, dispatchActive] = React.useReducer(activeReducer, defaultActiveProduct);

  useEffect(() => {
    (async function () {
      const response = await fetch(url, { method: 'GET' });
      dispatchProducts({ type: 'GET', payload: await response.json()})
    })()
  }, [url]);


  const getHandler = async () => {
      const response = await fetch(url, { method: 'GET' });
      dispatchProducts({ type: 'GET', payload: await response.json()})
  };

  const deleteProductHandler = async (product: Product) => {
    const response = await fetch(`${url}/${product.id}`, { method: 'DELETE' });
    if (response ) {
      await response.json()
    }
    if (response.ok) {
      dispatchProducts({ type: 'DELETE', payload: product})
      if (product.id === active.id) {
        setActive({...defaultActiveProduct});
      }
    }
  };

  const saveProductHandler = (product: Product) => {
    if (active.id) {
      editProductHandler(product);
    } else {
      addProductHandler(product);
    }
  };

  const addProductHandler = async (product: Product) => {
    const response = await fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      dispatchProducts({ type: 'ADD', payload: await response.json()})
      setReset();
    }
  };

  const editProductHandler = async (product: Product) => {
    const response = await fetch(`${url}/${product.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      dispatchProducts({ type: 'EDIT', payload: await response.json()})
    }
  };

  const setActive = (product: Product) => {
    dispatchActive({ type: 'SET_ACTIVE', payload: product});
  };

  const setReset = () => {
    dispatchActive({ type: 'SET_ACTIVE', payload: {...defaultActiveProduct}});
  };


  return {
    active,
    products,
    setActive,
    setReset,
    http: {
      get: getHandler,
      delete: deleteProductHandler,
      save: saveProductHandler,
    }
  }
}
