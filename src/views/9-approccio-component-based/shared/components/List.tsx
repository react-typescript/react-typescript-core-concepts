import React from 'react'
import { Product } from '../model/product';
import classNames from 'classnames';

interface ListProps {
  active: Product;
  product: Product[];
  onDelete: (u: Product) => void;
  onSetActive: (u: Product) => void;
}
export const List: React.FC<ListProps> = ({ active, product, onDelete, onSetActive }) => {

  const getTotal = () => {
    return product.reduce((acc: number, item: Product) => {
      return acc + item.price;
    }, 0)
  };

  const onDeleteHandler = (e: React.MouseEvent<HTMLElement>, item: Product) => {
    e.stopPropagation();
    onDelete(item);
  };

  return (
    <div>
      {
        product.map(item => {
          const selected = item.id === active.id
          return (
            <li
              key={item.id}
              className={classNames('list-group-item', { 'active': selected})}
              onClick={() => onSetActive(item)}
            >
              {item.name}
              <div className="pull-right">
                <div className="badge badge-dark mr-2">€ {item.price}</div>
                <i className="fa fa-trash" onClick={(e) => onDeleteHandler(e, item)} />
              </div>
            </li>
          )
        })
      }

      <div className="pull-right mr-4">
        <h4 className="badge badge-info">TOTAL: € {getTotal()}</h4>
      </div>
      <br/>

    </div>
  )
};
