import { User } from '../model/user';

export const data: User[] = [
  { id: 1, name: 'Fabio', tweets: 1010},
  { id: 2, name: 'Lorenzo', tweets: 10},
  { id: 3, name: 'Silvia', tweets: 5},
];
