import React, { useEffect } from 'react';

interface Product {
  id: number;
}

export const Demo10UseState: React.FC = () => {
  const [items, setItems] = React.useState<Product[]>([]);

  useEffect(() => {
    (async function() {
      const response = await fetch('http://localhost:3001/products', { method: 'GET' });
      console.log(await response.json())
    })();
  },[]);

  useEffect(() => {
    (async function() {
      const response = await fetch('http://localhost:3001/products', { method: 'GET' });
      setItems(await response.json());
    })();
  },[]);

  return  <h1>
    {items.length} Products
  </h1>
};

