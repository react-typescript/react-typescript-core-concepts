import React, { useReducer } from 'react';

const initialValue: Product = {
  qty: 0,
  available: false
};

interface Product {
  qty: number;
  available: boolean;
}

interface Action {
  type: string;
  payload?: Product;
}

function reducer(state: Product, action: Action) {
  switch (action.type) {
    case 'increment':
      return { available: true, qty: state.qty + 1 };
    case 'decrement':
      if (state.qty > 0) {
        const qty = state.qty - 1;
        const available = !(qty === 0);
        return {available, qty};
      }
      return state;
    case 'reset':
      return initialValue;
    default:
      throw new Error();
  }
}

export const Demo1UseReducer: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, initialValue);
  return (
    <>
      <div>Qty: {state.qty} </div>
      <div>Available: { state.available.toString()}</div>
      <hr/>
      <button
        onClick={() => dispatch({type: 'reset', payload: initialValue})}
      >
        Reset
      </button>
      <button onClick={() => dispatch({type: 'decrement'})}>-</button>
      <button onClick={() => dispatch({type: 'increment'})}>+</button>
    </>
  );
};

