import { RouteComponentProps } from 'react-router';
import React from 'react';

type TParams = { id: string };

export function ProductDetails({ match }: RouteComponentProps<TParams>) {
  console.log(match);
  return (<div>
    <h3>Product Details</h3>
    <pre>Product ID (params.id): {match.params.id}</pre>
    <pre>Route URL (url): {match.url}</pre>
    <pre>Route Rule (path): {match.path}</pre>
  </div>);
}
